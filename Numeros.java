import java.util.Scanner;
public class Numeros{
	Scanner leer = new Scanner(System.in);
	public void pedirDatos(){
		double a,b;
		System.out.println("Ingrese un numero decimal");
		a = leer.nextDouble();
		System.out.println("Ingrese otro numero decimal");
		b = leer.nextDouble();
		resolver (a,b);
	}
	public void resolver (double a, double b){
		double c;
		c = a+b;
		resultado (c);
	}
	public void resultado (double c){
		System.out.println("Resultado de la suma " + c);
	}
	public static void main(String[] args){
		Numeros n = new Numeros();
		n.pedirDatos();
	}
}